﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4
{
    public partial class Form1 : Form
    {   
    

        bool player1 = true, player2 = false;
        int win1 = 0, win2 = 0;
        int turns = 0;

        void ChangePlayer()
        {
            if (player1 == true)
            {
                player1 = false;
                player2 = true;
                label_player.Text = "Igrač broj 2 je na potezu";
            }
            else if(player2== true)
            {
                player2 = false;
                player1 = true;
                label_player.Text = "Igrač broj 1 je na potezu";
            }
        }

        void EnableButtons()
        {
            foreach(Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                }
                catch { }
            }
        }
        void CheckWinner()
        {
            bool winner = false;
            if (turns >= 5)
            {
                if ((button1.Text == button5.Text) && (button5.Text == button9.Text) && (!button1.Enabled))
                    winner = true;
                else if ((button3.Text == button5.Text) && (button5.Text == button7.Text)&&(!button3.Enabled))
                    winner = true;

                else if ((button1.Text == button2.Text) && (button3.Text == button1.Text)&&(!button1.Enabled))
                    winner = true;
                else if ((button4.Text == button5.Text) && (button5.Text == button6.Text) && (!button4.Enabled))
                    winner = true;
                else if ((button7.Text == button8.Text) && (button8.Text == button9.Text) && (!button7.Enabled))
                    winner = true;

                else if ((button1.Text == button4.Text) && (button4.Text == button7.Text) && (!button1.Enabled))
                    winner = true;
                else if ((button2.Text == button5.Text) && (button5.Text == button8.Text) && (!button2.Enabled))
                    winner = true;
                else if ((button3.Text == button6.Text) && (button6.Text == button9.Text) && (!button3.Enabled))
                    winner = true;
            }
            if (winner == true)
            {
                if (player1 == true)
                {
                    MessageBox.Show("Igrač broj 1 je pobjedio !");
                    win1++;
                    label_wins1.Text = win1.ToString();
                }
                else if (player2 == true)
                {
                    MessageBox.Show("Igrač broj 2 je pobjedio !");
                    win2++;
                    label_wins2.Text = win2.ToString();
                }
            }
            if (turns == 9)
            {
                MessageBox.Show("Izjednačeno!");
            }
        }
        public Form1()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (player1 == true)
            {
                b.Text = "X";
                turns++;
                CheckWinner();
                ChangePlayer();
                b.Enabled = false;
            }
            else if (player2 == true)
            {
                b.Text = "O";
                turns++;
                CheckWinner();
                ChangePlayer();
                b.Enabled = false;
            }

        }


        private void button_izlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button_start_Click(object sender, EventArgs e)
        {
            turns = 0;
            EnableButtons();
            button1.Text = "";
            button2.Text = "";
            button3.Text = "";
            button4.Text = "";
            button5.Text = "";
            button6.Text = "";
            button7.Text = "";
            button8.Text = "";
            button9.Text = "";
            player1 = true;
            player2 = false;
            label_player.Text= "Igrač broj 1 je na potezu";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label_wins1.Text = win1.ToString();
            label_wins2.Text = win2.ToString();
            label_player.Text = "Igrač broj 1 je na potezu";

        }
    }
}
