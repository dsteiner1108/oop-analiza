﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        double op1 = 0, op2 = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void but_izlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void but_odu_Click(object sender, EventArgs e)
        {
            if (textBox_op1.Text == "" || textBox_op2.Text == "")
                MessageBox.Show("Jedan od operanada nije unesen");
            else if (!double.TryParse(textBox_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!");
            else if (!double.TryParse(textBox_op2.Text, out op2))
                MessageBox.Show("Pogresan unos operanda 2!");
            else
            {
                label_rez.Text = (op1 - op2).ToString();
            }
        }

        private void but_mno_Click(object sender, EventArgs e)
        {
            if (textBox_op1.Text == "" || textBox_op2.Text == "")
                MessageBox.Show("Jedan od operanada nije unesen");
            else if (!double.TryParse(textBox_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!");
            else if (!double.TryParse(textBox_op2.Text, out op2))
                MessageBox.Show("Pogresan unos operanda 2!");
            else
            {
                label_rez.Text = (op1 * op2).ToString();
            }
        }

        private void but_dije_Click(object sender, EventArgs e)
        {
            if (textBox_op1.Text == "" || textBox_op2.Text == "")
                MessageBox.Show("Jedan od operanada nije unesen");
            else if (!double.TryParse(textBox_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!");
            else if (!double.TryParse(textBox_op2.Text, out op2))
                MessageBox.Show("Pogresan unos operanda 2!");
            else
            {
                label_rez.Text = (op1 / op2).ToString();
            }
        }

        private void but_sin_Click(object sender, EventArgs e)
        {
            if (textBox_op1.Text == "")
                MessageBox.Show("Prvi operand nije unesen");
            else if (!double.TryParse(textBox_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!");
            else
            {
                label_rez.Text = Math.Sin(op1).ToString();
            }
        }

        private void but_cos_Click(object sender, EventArgs e)
        {
            if (textBox_op1.Text == "")
                MessageBox.Show("Prvi operand nije unesen");
            else if (!double.TryParse(textBox_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!");
            else
            {
                label_rez.Text =Math.Cos(op1).ToString();
            }
        }

        private void but_log_Click(object sender, EventArgs e)
        {
            if (textBox_op1.Text == "")
                MessageBox.Show("Prvi operand nije unesen");
            else if (!double.TryParse(textBox_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!");
            else
            {
                label_rez.Text =Math.Log10(op1).ToString();
            }
        }

        private void but_sqrt_Click(object sender, EventArgs e)
        {
            if (textBox_op1.Text == "")
                MessageBox.Show("Prvi operand nije unesen");
            else if (!double.TryParse(textBox_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!");
            else
            {
                label_rez.Text = Math.Sqrt(op1).ToString();
            }
        }

        private void but_pot_Click(object sender, EventArgs e)
        {
            if (textBox_op1.Text == "")
                MessageBox.Show("Prvi operand nije unesen");
            else if (!double.TryParse(textBox_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!");
            else
            {
                label_rez.Text = Math.Pow(op1,2).ToString();
            }
        }

        private void but_naop2_Click(object sender, EventArgs e)
        {
            if (textBox_op1.Text == "" || textBox_op2.Text == "")
                MessageBox.Show("Jedan od operanada nije unesen");
            else if (!double.TryParse(textBox_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!");
            else if (!double.TryParse(textBox_op2.Text, out op2))
                MessageBox.Show("Pogresan unos operanda 2!");
            else
            {
                label_rez.Text = Math.Pow(op1,op2).ToString();
            }
        }

        private void but_brsve_Click(object sender, EventArgs e)
        {
            textBox_op1.Text = "";
            textBox_op2.Text = "";
            label_rez.Text = "";
        }

        private void but_zbr_Click(object sender, EventArgs e)
        {   if (textBox_op1.Text == "" || textBox_op2.Text == "")
                MessageBox.Show("Jedan od operanada nije unesen");
            else if (!double.TryParse(textBox_op1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!");
            else if (!double.TryParse(textBox_op2.Text, out op2))
                MessageBox.Show("Pogresan unos operanda 2!");
            else
            {
                label_rez.Text = (op1 + op2).ToString();
            }
        }
    }
}
