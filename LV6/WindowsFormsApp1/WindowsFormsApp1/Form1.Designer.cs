﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.but_dije = new System.Windows.Forms.Button();
            this.but_mno = new System.Windows.Forms.Button();
            this.but_odu = new System.Windows.Forms.Button();
            this.but_zbr = new System.Windows.Forms.Button();
            this.but_sqrt = new System.Windows.Forms.Button();
            this.but_pot = new System.Windows.Forms.Button();
            this.but_sin = new System.Windows.Forms.Button();
            this.but_cos = new System.Windows.Forms.Button();
            this.but_log = new System.Windows.Forms.Button();
            this.but_brsve = new System.Windows.Forms.Button();
            this.but_natre = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_op1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_op2 = new System.Windows.Forms.TextBox();
            this.label_rez = new System.Windows.Forms.Label();
            this.but_izlaz = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // but_dije
            // 
            this.but_dije.Location = new System.Drawing.Point(139, 113);
            this.but_dije.Name = "but_dije";
            this.but_dije.Size = new System.Drawing.Size(35, 29);
            this.but_dije.TabIndex = 14;
            this.but_dije.Text = "/";
            this.but_dije.UseVisualStyleBackColor = true;
            this.but_dije.Click += new System.EventHandler(this.but_dije_Click);
            // 
            // but_mno
            // 
            this.but_mno.Location = new System.Drawing.Point(139, 161);
            this.but_mno.Name = "but_mno";
            this.but_mno.Size = new System.Drawing.Size(35, 29);
            this.but_mno.TabIndex = 15;
            this.but_mno.Text = "*";
            this.but_mno.UseVisualStyleBackColor = true;
            this.but_mno.Click += new System.EventHandler(this.but_mno_Click);
            // 
            // but_odu
            // 
            this.but_odu.Location = new System.Drawing.Point(139, 207);
            this.but_odu.Name = "but_odu";
            this.but_odu.Size = new System.Drawing.Size(35, 29);
            this.but_odu.TabIndex = 16;
            this.but_odu.Text = "-";
            this.but_odu.UseVisualStyleBackColor = true;
            this.but_odu.Click += new System.EventHandler(this.but_odu_Click);
            // 
            // but_zbr
            // 
            this.but_zbr.Location = new System.Drawing.Point(139, 251);
            this.but_zbr.Name = "but_zbr";
            this.but_zbr.Size = new System.Drawing.Size(35, 29);
            this.but_zbr.TabIndex = 17;
            this.but_zbr.Text = "+";
            this.but_zbr.UseVisualStyleBackColor = true;
            this.but_zbr.Click += new System.EventHandler(this.but_zbr_Click);
            // 
            // but_sqrt
            // 
            this.but_sqrt.Location = new System.Drawing.Point(89, 113);
            this.but_sqrt.Name = "but_sqrt";
            this.but_sqrt.Size = new System.Drawing.Size(35, 29);
            this.but_sqrt.TabIndex = 18;
            this.but_sqrt.Text = "sqrt";
            this.but_sqrt.UseVisualStyleBackColor = true;
            this.but_sqrt.Click += new System.EventHandler(this.but_sqrt_Click);
            // 
            // but_pot
            // 
            this.but_pot.Location = new System.Drawing.Point(35, 205);
            this.but_pot.Name = "but_pot";
            this.but_pot.Size = new System.Drawing.Size(35, 29);
            this.but_pot.TabIndex = 19;
            this.but_pot.Text = "^2";
            this.but_pot.UseVisualStyleBackColor = true;
            this.but_pot.Click += new System.EventHandler(this.but_pot_Click);
            // 
            // but_sin
            // 
            this.but_sin.Location = new System.Drawing.Point(89, 251);
            this.but_sin.Name = "but_sin";
            this.but_sin.Size = new System.Drawing.Size(35, 29);
            this.but_sin.TabIndex = 20;
            this.but_sin.Text = "sin";
            this.but_sin.UseVisualStyleBackColor = true;
            this.but_sin.Click += new System.EventHandler(this.but_sin_Click);
            // 
            // but_cos
            // 
            this.but_cos.Location = new System.Drawing.Point(89, 207);
            this.but_cos.Name = "but_cos";
            this.but_cos.Size = new System.Drawing.Size(35, 29);
            this.but_cos.TabIndex = 21;
            this.but_cos.Text = "cos";
            this.but_cos.UseVisualStyleBackColor = true;
            this.but_cos.Click += new System.EventHandler(this.but_cos_Click);
            // 
            // but_log
            // 
            this.but_log.Location = new System.Drawing.Point(89, 161);
            this.but_log.Name = "but_log";
            this.but_log.Size = new System.Drawing.Size(35, 29);
            this.but_log.TabIndex = 22;
            this.but_log.Text = "log";
            this.but_log.UseVisualStyleBackColor = true;
            this.but_log.Click += new System.EventHandler(this.but_log_Click);
            // 
            // but_brsve
            // 
            this.but_brsve.Location = new System.Drawing.Point(35, 113);
            this.but_brsve.Name = "but_brsve";
            this.but_brsve.Size = new System.Drawing.Size(35, 29);
            this.but_brsve.TabIndex = 23;
            this.but_brsve.Text = "CE";
            this.but_brsve.UseVisualStyleBackColor = true;
            this.but_brsve.Click += new System.EventHandler(this.but_brsve_Click);
            // 
            // but_natre
            // 
            this.but_natre.Location = new System.Drawing.Point(35, 161);
            this.but_natre.Name = "but_natre";
            this.but_natre.Size = new System.Drawing.Size(35, 29);
            this.but_natre.TabIndex = 25;
            this.but_natre.Text = "x^Y";
            this.but_natre.UseVisualStyleBackColor = true;
            this.but_natre.Click += new System.EventHandler(this.but_naop2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "Operand 1:";
            // 
            // textBox_op1
            // 
            this.textBox_op1.Location = new System.Drawing.Point(24, 31);
            this.textBox_op1.Name = "textBox_op1";
            this.textBox_op1.Size = new System.Drawing.Size(135, 20);
            this.textBox_op1.TabIndex = 27;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 28;
            this.label2.Text = "Operand 2:";
            // 
            // textBox_op2
            // 
            this.textBox_op2.Location = new System.Drawing.Point(24, 70);
            this.textBox_op2.Name = "textBox_op2";
            this.textBox_op2.Size = new System.Drawing.Size(135, 20);
            this.textBox_op2.TabIndex = 29;
            // 
            // label_rez
            // 
            this.label_rez.AutoSize = true;
            this.label_rez.Location = new System.Drawing.Point(81, 300);
            this.label_rez.Name = "label_rez";
            this.label_rez.Size = new System.Drawing.Size(0, 13);
            this.label_rez.TabIndex = 30;
            // 
            // but_izlaz
            // 
            this.but_izlaz.Location = new System.Drawing.Point(29, 251);
            this.but_izlaz.Name = "but_izlaz";
            this.but_izlaz.Size = new System.Drawing.Size(54, 29);
            this.but_izlaz.TabIndex = 31;
            this.but_izlaz.Text = "Izlaz";
            this.but_izlaz.UseVisualStyleBackColor = true;
            this.but_izlaz.Click += new System.EventHandler(this.but_izlaz_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 300);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 32;
            this.label3.Text = "Rezultat:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(208, 332);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.but_izlaz);
            this.Controls.Add(this.label_rez);
            this.Controls.Add(this.textBox_op2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_op1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.but_natre);
            this.Controls.Add(this.but_brsve);
            this.Controls.Add(this.but_log);
            this.Controls.Add(this.but_cos);
            this.Controls.Add(this.but_sin);
            this.Controls.Add(this.but_pot);
            this.Controls.Add(this.but_sqrt);
            this.Controls.Add(this.but_zbr);
            this.Controls.Add(this.but_odu);
            this.Controls.Add(this.but_mno);
            this.Controls.Add(this.but_dije);
            this.Name = "Form1";
            this.Text = "Kalkulator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button but_dije;
        private System.Windows.Forms.Button but_mno;
        private System.Windows.Forms.Button but_odu;
        private System.Windows.Forms.Button but_zbr;
        private System.Windows.Forms.Button but_sqrt;
        private System.Windows.Forms.Button but_pot;
        private System.Windows.Forms.Button but_sin;
        private System.Windows.Forms.Button but_cos;
        private System.Windows.Forms.Button but_log;
        private System.Windows.Forms.Button but_brsve;
        private System.Windows.Forms.Button but_natre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_op1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_op2;
        private System.Windows.Forms.Label label_rez;
        private System.Windows.Forms.Button but_izlaz;
        private System.Windows.Forms.Label label3;
    }
}

