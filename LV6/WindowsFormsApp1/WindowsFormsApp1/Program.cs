﻿//1. Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost znanstvenog kalkulatora, 
//odnosno implementirati osnovne(+,-,*,/) i barem 5 naprednih(sin, cos, log, sqrt...) operacija.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
