﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Vjesala : Form
    {
        class Pojam
        {
            private string rijec;
            public Pojam()
            {
                rijec = " ";
            }

            public Pojam(string p)
            {
                rijec = p;
            }

            public override string ToString()
            {
                return rijec;
            }

            public string Rijec
            {
                get
                {
                    return rijec;
                }

                set
                {
                    rijec = value;
                }
            }
        }

        List<Pojam> listPojam = new List<Pojam>();
        List<char> poja = new List<char>();
        char slovo;
        string pop;
        int pok = 10;

        public Vjesala()
        {
            InitializeComponent();
        }

        private void Vjesala_Load(object sender, EventArgs e)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader("Pojmovi.txt"))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    Pojam p = new Pojam(line);
                    listPojam.Add(p);
                }
            }

            
            Random a = new Random();
            int mIndex = a.Next(listPojam.Capacity);
            pop = listPojam[mIndex].ToString();
            poja.Capacity = pop.Length; 
            for(int i = 0; i < pop.Length; i++)
            {
                poja.Add('-');
            }
            lab_pojam.Text = new string(poja.ToArray());
            lab_pok.Text = pok.ToString();
            lab_brsl.Text = "Broj slova je: " + pop.Length.ToString();
        }
        

        private void butt_slovo_Click(object sender, EventArgs e)
        {
            bool empty = false, parse = false;
            if (tb_slovo.Text == "")
            {
                MessageBox.Show("Polje za unos slova je prazno");
                empty = true;
            }
            else
            {
                empty = false;
            }
            if (empty == false)
            {
                if (!char.TryParse(tb_slovo.Text, out slovo))
                {
                    MessageBox.Show("Pogresan unos slova");
                    parse = true;
                }
                else
                {
                    parse = false;
                }
            }

           

            if (!empty && !parse)
            {
                if ((pop.Contains(slovo)))
                {
                    for (int i = 0; i < pop.Length; i++)
                    {
                        if (pop[i] == slovo)
                        {
                            poja[i] = slovo;

                        }

                    }
                    lab_pojam.Text = new string(poja.ToArray());
                }
                else
                {
                    pok--;
                    lab_pok.Text = pok.ToString();
                }

                if (pok == 0)
                {
                    MessageBox.Show("Obješeni ste", "GAME OVER!");
                    Application.Exit();
                }
                if (pop == new string(poja.ToArray()))
                {
                    MessageBox.Show("Cestitam, Pobjedili ste","POBJEDA!");
                    Application.Exit();
                }
            }
            tb_slovo.Clear();
        }

        private void butt_pojam_Click(object sender, EventArgs e)
        {
            bool empty = false;
            if (tB_pojam.Text == "")
            {
                MessageBox.Show("Polje za unos pojma je prazno");
                empty = true;
            }
            else
            {
                empty = false;
            }

            if (empty == false)
            {
                if (tB_pojam.Text == pop)
                {
                    lab_pojam.Text = pop;
                    MessageBox.Show("Cestitam, Pobjedili ste", "POBJEDA!");
                    Application.Exit();
                }
                else
                {
                    pok--;
                    lab_pok.Text = pok.ToString();
                }

                if (pok == 0)
                {
                    MessageBox.Show("Obješeni ste", "GAME OVER!");
                    Application.Exit();
                }

            }
            tB_pojam.Clear();
        }

        private void butt_izlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void butt_predaja_Click(object sender, EventArgs e)
        {
            lab_pojam.Text = pop;
            MessageBox.Show("Predali ste se", "GAME OVER!");
            Application.Exit();
        }
    }
}