﻿namespace WindowsFormsApp2
{
    partial class Vjesala
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_slovo = new System.Windows.Forms.TextBox();
            this.tB_pojam = new System.Windows.Forms.TextBox();
            this.butt_slovo = new System.Windows.Forms.Button();
            this.butt_pojam = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.lab_pojam = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lab_pok = new System.Windows.Forms.Label();
            this.butt_predaja = new System.Windows.Forms.Button();
            this.butt_izlaz = new System.Windows.Forms.Button();
            this.lab_brsl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pogodi slovo: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Pogodi pojam: ";
            // 
            // tb_slovo
            // 
            this.tb_slovo.Location = new System.Drawing.Point(136, 37);
            this.tb_slovo.Name = "tb_slovo";
            this.tb_slovo.Size = new System.Drawing.Size(100, 20);
            this.tb_slovo.TabIndex = 2;
            // 
            // tB_pojam
            // 
            this.tB_pojam.Location = new System.Drawing.Point(136, 92);
            this.tB_pojam.Name = "tB_pojam";
            this.tB_pojam.Size = new System.Drawing.Size(100, 20);
            this.tB_pojam.TabIndex = 3;
            // 
            // butt_slovo
            // 
            this.butt_slovo.Location = new System.Drawing.Point(267, 24);
            this.butt_slovo.Name = "butt_slovo";
            this.butt_slovo.Size = new System.Drawing.Size(75, 41);
            this.butt_slovo.TabIndex = 4;
            this.butt_slovo.Text = "Pogodi slovo!";
            this.butt_slovo.UseVisualStyleBackColor = true;
            this.butt_slovo.Click += new System.EventHandler(this.butt_slovo_Click);
            // 
            // butt_pojam
            // 
            this.butt_pojam.Location = new System.Drawing.Point(267, 82);
            this.butt_pojam.Name = "butt_pojam";
            this.butt_pojam.Size = new System.Drawing.Size(75, 38);
            this.butt_pojam.TabIndex = 5;
            this.butt_pojam.Text = "Pogodi pojam!";
            this.butt_pojam.UseVisualStyleBackColor = true;
            this.butt_pojam.Click += new System.EventHandler(this.butt_pojam_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 181);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Vaš pojam: ";
            // 
            // lab_pojam
            // 
            this.lab_pojam.AutoSize = true;
            this.lab_pojam.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lab_pojam.Location = new System.Drawing.Point(154, 181);
            this.lab_pojam.Name = "lab_pojam";
            this.lab_pojam.Size = new System.Drawing.Size(0, 18);
            this.lab_pojam.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(39, 217);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 26);
            this.label4.TabIndex = 8;
            this.label4.Text = "Preostali broj \r\n   pokušaja: ";
            // 
            // lab_pok
            // 
            this.lab_pok.AutoSize = true;
            this.lab_pok.Location = new System.Drawing.Point(154, 230);
            this.lab_pok.Name = "lab_pok";
            this.lab_pok.Size = new System.Drawing.Size(0, 13);
            this.lab_pok.TabIndex = 9;
            // 
            // butt_predaja
            // 
            this.butt_predaja.Location = new System.Drawing.Point(41, 274);
            this.butt_predaja.Name = "butt_predaja";
            this.butt_predaja.Size = new System.Drawing.Size(75, 23);
            this.butt_predaja.TabIndex = 10;
            this.butt_predaja.Text = "Predaja";
            this.butt_predaja.UseVisualStyleBackColor = true;
            this.butt_predaja.Click += new System.EventHandler(this.butt_predaja_Click);
            // 
            // butt_izlaz
            // 
            this.butt_izlaz.Location = new System.Drawing.Point(267, 274);
            this.butt_izlaz.Name = "butt_izlaz";
            this.butt_izlaz.Size = new System.Drawing.Size(75, 23);
            this.butt_izlaz.TabIndex = 11;
            this.butt_izlaz.Text = "Izlaz";
            this.butt_izlaz.UseVisualStyleBackColor = true;
            this.butt_izlaz.Click += new System.EventHandler(this.butt_izlaz_Click);
            // 
            // lab_brsl
            // 
            this.lab_brsl.AutoSize = true;
            this.lab_brsl.Location = new System.Drawing.Point(249, 181);
            this.lab_brsl.Name = "lab_brsl";
            this.lab_brsl.Size = new System.Drawing.Size(0, 13);
            this.lab_brsl.TabIndex = 12;
            // 
            // Vjesala
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(354, 309);
            this.Controls.Add(this.lab_brsl);
            this.Controls.Add(this.butt_izlaz);
            this.Controls.Add(this.butt_predaja);
            this.Controls.Add(this.lab_pok);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lab_pojam);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.butt_pojam);
            this.Controls.Add(this.butt_slovo);
            this.Controls.Add(this.tB_pojam);
            this.Controls.Add(this.tb_slovo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Vjesala";
            this.Text = "Vjesala";
            this.Load += new System.EventHandler(this.Vjesala_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_slovo;
        private System.Windows.Forms.TextBox tB_pojam;
        private System.Windows.Forms.Button butt_slovo;
        private System.Windows.Forms.Button butt_pojam;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lab_pojam;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lab_pok;
        private System.Windows.Forms.Button butt_predaja;
        private System.Windows.Forms.Button butt_izlaz;
        private System.Windows.Forms.Label lab_brsl;
    }
}

