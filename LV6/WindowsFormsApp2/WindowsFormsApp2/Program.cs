﻿//2. Napravite jednostavnu igru vješala.Pojmovi se učitavaju u listu iz datoteke, i u svakoj partiji se odabire
//nasumični pojam iz liste. Omogućiti svu funkcionalnost koju biste očekivali od takve igre.
//Nije nužno crtati vješala, dovoljno je na labeli ispisati koliko je pokušaja za odabir slova preostalo.


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>



        [STAThread]
        static void Main()
        {
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Vjesala());

        }
    }
}
